﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraController : MonoBehaviour
{
    private Vector3 mouseOriginPoint;
    private Vector3 mouseOffset;
    bool isDragging;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Scroll wheel zoom
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize -
                Input.GetAxis("Mouse ScrollWheel") * Camera.main.orthographicSize, 1.5f, 15f);
        }

        // Middle mouse button drag to pan
        if (Input.GetMouseButton(2))
        {
            mouseOffset = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position);
            if (!isDragging)
            {
                isDragging = true;
                mouseOriginPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
        }
        else
            isDragging = false;

        if (isDragging)
            transform.position = mouseOriginPoint - mouseOffset;
    }
}
