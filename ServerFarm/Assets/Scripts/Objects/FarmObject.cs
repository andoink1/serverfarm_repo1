﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FarmObject : MonoBehaviour
{
    //public string name;
    public int id;
    public int sizeX;
    public int sizeY;
    public int sizeZ;
    public Vector3 direction;
    public bool highlightPlacement;

    public int cost;
}
