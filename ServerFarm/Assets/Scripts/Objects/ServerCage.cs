﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerCage : FarmObject
{
    public int serverCageCost = 100;
    public float borderSize = 0.3f;
    public float gapSize = 0.2f;
    public float unitSize = 1.0f;
    public ServerUnit[] serverUnits;

    private int serverCount = 6;

    // Start is called before the first frame update
    void Start()
    {
        cost = serverCageCost;

        serverUnits = new ServerUnit[serverCount];
        for (int i = 0; i < serverCount; i++)
            serverUnits[i] = null;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddServerUnit(ServerUnit serverUnit)
    {
        for (int i=0; i<serverCount; i++)
        {
            if (serverUnits[i] == null)
            {
                // Offset X to protrude from front of cage
                Vector3 unitPosition = this.transform.position;
                unitPosition.x -= 0.2f;

                // Offset Y to position among others
                float yOffset = ((serverCount - i - 1) * (gapSize + unitSize)) 
                    + (unitSize / 2.0f) + borderSize;
                unitPosition.y = yOffset;

                ServerUnit createdUnit = Instantiate(serverUnit, unitPosition, Quaternion.identity, this.transform.parent);
                serverUnits[i] = createdUnit;

                break;
            }
        }
    }
}
