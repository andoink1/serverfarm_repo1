﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarmBoard : MonoBehaviour
{
    public int farmX;
    public int farmZ;
    private FarmObject[,] farmObjects;

    // Start is called before the first frame update
    void Start()
    {
        farmObjects = new FarmObject[farmX, farmZ];
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Create FarmObject at position and set grid.
    /// </summary>
    /// <param name="farmObject"></param>
    /// <param name="position"></param>
    public void AddFarmObject(FarmObject farmObject, Vector3 position)
    {
        Vector3 offsetBySize = new Vector3(
            farmObject.sizeX / 2f,
            farmObject.sizeY / 2f,
            farmObject.sizeZ / 2f);
        Vector3 positionWithOffset = position + offsetBySize;

        FarmObject farmObjectToAdd = Instantiate(farmObject, positionWithOffset, Quaternion.identity);
        GameObject serverParent = new GameObject("Server");
        farmObjectToAdd.transform.parent = serverParent.transform;

        // Set FarmObject on covered grid coordinates
        for (int x = (int)position.x; x < (int)position.x + farmObject.sizeX; x++)
        {
            for (int z = (int)position.z; z < (int)position.z + farmObject.sizeZ; z++)
            {
                farmObjects[x, z] = farmObjectToAdd;
                Debug.Log("AddFarmObject: " + farmObject.name + " [" + x + ", " + z + "]");
            }
        }

        //farmObjects[(int)position.x, (int)position.z] = farmObjectToAdd;
    }

    /// <summary>
    /// Delete FarmObject at grid coordinates.
    /// </summary>
    /// <param name="position"></param>
    public void RemoveFarmObjectAtPosition(Vector3 position)
    {
        Debug.Log("RemoveFarmObjectAtPosition: " + farmObjects[(int)position.x, (int)position.z].name + " [" + position.x + ", " + position.z + "]");
        Destroy(farmObjects[(int)position.x, (int)position.z].gameObject);
        ClearFarmObjectGridRange(
            (int)position.x, 
            (int)position.z,
            farmObjects[(int)position.x, (int)position.z].sizeX,
            farmObjects[(int)position.x, (int)position.z].sizeZ);
    }

    /// <summary>
    /// Find and delete input FarmObject from board. 
    /// </summary>
    /// <param name="farmObject"></param>
    public void RemoveFarmObject(FarmObject farmObject)
    {
        // Find FarmObject within grid
        for (int x = 0; x < farmX; x++)
        {
            for (int z = 0; z < farmZ; z++)
            {
                if (farmObject.Equals(farmObjects[x, z]))
                {
                    Debug.Log("RemoveFarmObject: " + farmObjects[x, z].name + " [" + x + ", " + z + "]");
                    Destroy(farmObjects[x, z].gameObject);
                    ClearFarmObjectGridRange(x, z, farmObjects[x, z].sizeX, farmObjects[x, z].sizeZ);
                    return;
                }
            }
        }
    }

    /// <summary>
    /// Return grid coordinates from input position.
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public Vector3 CalculateGridPosition(Vector3 position)
    {
        return new Vector3(Mathf.Floor(position.x), 0f, Mathf.Floor(position.z));
    }

    /// <summary>
    /// Return FarmObject at grid coordinates.
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public FarmObject GetFarmObjectAtPosition(Vector3 position)
    {
        return farmObjects[(int)position.x, (int)position.z];
    }

    /// <summary>
    /// Check for FarmObject at given position within size range.
    /// </summary>
    /// <param name="position"></param>
    /// <param name="sizeX"></param>
    /// <param name="sizeZ"></param>
    /// <returns></returns>
    public FarmObject GetFarmObjectAtPositionWithSize(Vector3 position, int sizeX, int sizeZ)
    {
        // Search for FarmObject within range
        for (int x = (int)position.x; x < position.x + sizeX; x++)
        {
            for (int z = (int)position.z; z < position.z + sizeZ; z++)
            {
                if (farmObjects[(int)x, (int)z] != null)
                    return farmObjects[(int)x, (int)z];
            }
        }

        return null;
    }

    /// <summary>
    /// Sets range of farmObjects[] to null.
    /// </summary>
    /// <param name="startX"></param>
    /// <param name="startZ"></param>
    /// <param name="sizeX"></param>
    /// <param name="sizeZ"></param>
    public void ClearFarmObjectGridRange(int startX, int startZ, int sizeX, int sizeZ)
    {
        // Loop range and null each index
        for (int x = startX; x < startX + sizeX; x++)
        {
            for (int z = startZ; z < startZ + sizeZ; z++)
            {
                farmObjects[x, z] = null;
                //Debug.Log("ClearFarmObjectGridRange: [" + x + ", " + z + "]");
            }
        }
    }
}
