﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Farm : MonoBehaviour
{
    public int credits;
    public int day;

    // Start is called before the first frame update
    void Start()
    {
        credits = 500;
        day = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EndDay()
    {
        Debug.Log("End Day " + day);
        day++;
    }
}
