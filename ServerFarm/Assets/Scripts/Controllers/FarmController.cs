﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FarmController : MonoBehaviour
{
    public Text moneyText;

    private Farm farm;
    private GameObject farmObjectHighlighter;
    [SerializeField]
    private FarmObject[] farmObjects;
    private FarmBoard farmBoard;
    private FarmObject selectedFarmObject;

    // Start is called before the first frame update
    void Start()
    {
        farm = GetComponent<Farm>();
        farmBoard = farm.GetComponent<FarmBoard>();
        farmObjectHighlighter = GameObject.Find("PlacementHighlight");

        UpdateFarmMoney(0);

        // Set ids on FarmObjects
        for (int i=0; i<farmObjects.Length;i++)
            farmObjects[i].id = i;
    }

    // Update is called once per frame
    void Update()
    {
        if (selectedFarmObject != null && selectedFarmObject.highlightPlacement)
        {
            if (!farmObjectHighlighter.activeSelf)
                farmObjectHighlighter.SetActive(true);
            
            UpdateFarmObjectHighlighter(selectedFarmObject);
        }
        else
        {
            if (farmObjectHighlighter.activeSelf)
                farmObjectHighlighter.SetActive(false);
        }

        // Left-click with Shift to place multiple FarmObjects
        if (Input.GetMouseButton(0)
            && Input.GetKey(KeyCode.LeftShift)
            && selectedFarmObject != null)
            AddFarmObjectToBoard();
        // Left-click to place single FarmObject
        else if (Input.GetMouseButtonDown(0) 
            && selectedFarmObject != null)
            AddFarmObjectToBoard();
        // Right-click with Shift to delete multiple FarmObjects
        else if (Input.GetMouseButton(1)
            && Input.GetKey(KeyCode.LeftShift))
            RemoveFarmObjectFromBoard();
        // Right-click to delete FarmObject
        else if (Input.GetMouseButtonDown(1))
            RemoveFarmObjectFromBoard();
    }

    /// <summary>
    /// Set selected FarmObject.
    /// </summary>
    /// <param name="farmObjectId"></param>
    public void SelectFarmObject(int farmObjectId)
    {
        // If same FarmObject selected, de-select it
        // Otherwise, set selected FarmObject
        if (farmObjects[farmObjectId].Equals(selectedFarmObject))
            selectedFarmObject = null;
        else
            selectedFarmObject = farmObjects[farmObjectId];


        // TODO: Control button colors
        //UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);
        //if (farmObjectId == 1)
        //{
        //    var button = GameObject.Find("CreateButton4x4").GetComponent<UnityEngine.UI.Button>();
        //    var colors = button.colors;
        //    colors.normalColor = Color.red;
        //    button.colors = colors;
        //}
    }

    /// <summary>
    /// Add FarmObject to clicked position, if eligible.
    /// </summary>
    public void AddFarmObjectToBoard()
    {
        if (selectedFarmObject != null)
        {
            // ServerCage
            if (selectedFarmObject.id == 0)
            {
                Vector3? mouseGridPosition = MouseCastToGridPosition();

                if (mouseGridPosition != null)
                {
                    // If there is no FarmObject at this position, and the mouse is not over a GameObject
                    if (farmBoard.GetFarmObjectAtPositionWithSize((Vector3)mouseGridPosition, selectedFarmObject.sizeX, selectedFarmObject.sizeZ) == null
                        && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                    {
                        // Deduct cash, update data, increment building count, and add building
                        //uiController.UpdateCityData();
                        //city.buildingCounts[selectedBuilding.id]++;

                        if (farm.credits >= selectedFarmObject.cost)
                        {
                            UpdateFarmMoney(-selectedFarmObject.cost);
                            farmBoard.AddFarmObject(selectedFarmObject, (Vector3)mouseGridPosition);
                            return;
                        }
                    }
                }
            }
            // ServerUnit
            else if (selectedFarmObject.id == 1)
            {
                // Check for collision with ServerCage
                FarmObject hitFarmObject = MouseCastToFarmObject();
                if (hitFarmObject != null)
                {
                    ServerCage serverCage = hitFarmObject.GetComponent<ServerCage>();
                    if (serverCage != null)
                    {
                        if (farm.credits >= selectedFarmObject.cost)
                        {
                            UpdateFarmMoney(-selectedFarmObject.cost);
                            serverCage.AddServerUnit((ServerUnit)selectedFarmObject);
                        }
                    }
                    else
                    {
                        Debug.Log("AddFarmObjectToBoard: FarmObject NOT ServerCage!");
                    }
                    return;
                }
            }
        }
    }

    /// <summary>
    /// Remove FarmObject from clicked position, if applicable.
    /// </summary>
    public void RemoveFarmObjectFromBoard()
    {
        // Check for collision with FarmObject and delete
        FarmObject hitFarmObject = MouseCastToFarmObject();
        if (hitFarmObject != null)
        {
            UpdateFarmMoney(hitFarmObject.cost / 2);
            farmBoard.RemoveFarmObject(hitFarmObject);
            return;
        }

        // Otherwise, get grid position and delete FarmObject there
        Vector3? gridPosition = MouseCastToGridPosition();
        if (gridPosition != null)
        {
            if (farmBoard.GetFarmObjectAtPosition((Vector3)gridPosition) != null)
            {
                FarmObject farmObjectToRemove = farmBoard.GetFarmObjectAtPosition((Vector3)gridPosition);
                if (farmObjectToRemove != null)
                {
                    // Return half the cost
                    UpdateFarmMoney(farmObjectToRemove.cost / 2);

                    farmBoard.RemoveFarmObjectAtPosition((Vector3)gridPosition);
                    //uiController.UpdateCityData();

                    Debug.Log("Object removed");
                }
            }
        }
    }

    /// <summary>
    /// Find grid coordinates from mouse cast.
    /// </summary>
    /// <returns></returns>
    public Vector3? MouseCastToGridPosition()
    {
        Vector3? gridPosition = null;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
            gridPosition = farmBoard.CalculateGridPosition(hit.point);
       
        return gridPosition;
    }

    /// <summary>
    /// Find FarmObject collision from mouse cast.
    /// </summary>
    /// <returns></returns>
    public FarmObject MouseCastToFarmObject()
    {
        FarmObject farmObject = null;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            farmObject = hit.transform.gameObject.GetComponent<FarmObject>();
            return farmObject;
        }

        return null;
    }

    public void UpdateFarmObjectHighlighter(FarmObject farmObjectForHighlight)
    {
        if (!farmObjectHighlighter.activeSelf)
            return;

        Vector3? mousePosition = MouseCastToGridPosition();
        if (mousePosition == null)
        {
            farmObjectHighlighter.SetActive(false);
            return;
        }
        else
            farmObjectHighlighter.SetActive(true);

        // Highlighter size = 10f

        farmObjectHighlighter.transform.localScale = new Vector3(
            (float)farmObjectForHighlight.sizeX / 10f,
            1f,
            (float)farmObjectForHighlight.sizeZ / 10f);

        Vector3 offsetBySize = new Vector3(
            farmObjectForHighlight.sizeX / 2f,
            0.1f,
            farmObjectForHighlight.sizeZ / 2f);
        Vector3 positionWithOffset = (Vector3)mousePosition + offsetBySize;

        farmObjectHighlighter.transform.position = positionWithOffset;
    }

    public void UpdateFarmMoney(int moneyChange)
    {
        farm.credits += moneyChange;
        moneyText.text = "$" + farm.credits;
    }
}
